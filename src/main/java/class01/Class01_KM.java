package class01;

public class Class01_KM {

    public static int class01_KM(int[] arr, int k, int m) {
        int[] t = new int[32];
        for (int num : arr) {
            for (int i = 0; i < 32; i++) {
                t[i] += (num >> i) & 1;
            }
        }
        int ans = 0;
        for (int i = 0; i < 32; i++) {
            if (t[i] % m != 0) {
                ans += (int) Math.pow(2, i);
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {4, 4, 5, 5, 5, 5, 6, 6, 6, 6};
        System.out.println(class01_KM(arr, 2, 4));
    }
}
