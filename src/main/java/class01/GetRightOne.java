package class01;

public class GetRightOne {
    public static int getRightOne(int num){
        return num & (~num + 1);
    }


    public static void main(String[] args) {
        int a = 16;
        System.out.println(getRightOne(a));
    }
}
