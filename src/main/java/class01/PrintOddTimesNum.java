package class01;

public class PrintOddTimesNum {
   public static void  printOddTimesNum(int[] arr){
       int err = 0 ;
       if (arr == null) {
           return;
       }
       for(int i= 0;i < arr.length ;i++){
           err ^= arr[i] ;
       }
       System.out.println(err);
   }

   public static void printTwoOddTimesNums(int[] arr){
       int err = 0 ;
       int theOnlyOne = 0;
       if (arr == null) {
           return;
       }
       for(int i= 0;i < arr.length ;i++){
           err ^= arr[i] ;
       }
       int rightOne = err & (~err + 1);
       for(int i= 0;i < arr.length ;i++){
           if((arr[i] &  rightOne) != 0){
               theOnlyOne ^= arr[i] ;
           }
       }
       System.out.println(theOnlyOne + " " + (theOnlyOne ^ err));
   }

    public static void main(String[] args) {
        int[] arr ={10,4,4,5,5,5,5,6,6,7,7,7};
        //printOddTimesNum(arr);
        printTwoOddTimesNums(arr);

    }
}
