package class02;

public class RemoveValue {
    public static class Node{
        int value;
        Node next;

        public Node(int value){
            this.value = value;
        }
    }
    public static void printNode(Node head){
        while(head != null){
            System.out.print(head.value + " ");
            head = head.next;
        }
    }

    public static Node removeValue(Node head,int value){
        while(head.value == value){
            head = head.next;
        }
        Node pre= head;
        Node cur = head;
        while(cur != null){
            if(cur.value == value){
                pre.next = cur.next;
            }else{
                pre= cur;
            }
            cur = cur.next;
        }
        return head;
    }

    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3= new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        n1.next=n2;
        n1.next.next = n3;
        n1.next.next.next = n4;
        n1.next.next.next.next = n5;
        printNode(n1);
        System.out.println();
        Node head = removeValue(n1,4);
        printNode(head);
    }

}
