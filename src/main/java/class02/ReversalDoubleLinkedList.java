package class02;

public class ReversalDoubleLinkedList {
    public static class DoubleNode{
        int value;
        DoubleNode pre;
        DoubleNode next;
        
        public DoubleNode(int value){
            this.value = value;
        }
    }

    public static void printNode(DoubleNode head){
        while(head != null){
            System.out.print(head.value + " ");
            head = head.next;
        }
    }
    
    public static DoubleNode ReversalDoubleLinkedList(DoubleNode head){
        if (head == null) {
            return  null;
        }
        
        DoubleNode pre = null;
        DoubleNode next = null;
        while(head != null){
            next = head.next;
            head.next = pre;
            head.pre = next;
            pre= head;
            head= next;
        }
        return pre;
    }

    public static void main(String[] args) {
        DoubleNode n1 = new DoubleNode(1);
        DoubleNode n2 = new DoubleNode(2);
        DoubleNode n3= new DoubleNode(3);
        DoubleNode n4 = new DoubleNode(4);
        DoubleNode n5 = new DoubleNode(5);
        n1.next=n2;
        n1.next.next = n3;
        n1.next.next.next = n4;
        n1.next.next.next.next = n5;
        printNode(n1);
        DoubleNode head = ReversalDoubleLinkedList(n1);
        System.out.println();
        printNode(head);
    }
}
