package class02;

public class ReversalLinkedList {
    public static class Node{
        int value;
        Node next;

        public Node(int value){
            this.value = value;
        }
    }

    public static Node reversalListNode(Node head){
        if (head == null) {
            return null;
        }
        Node pre = null;
        Node next = null;
        while(head != null){
            next =  head.next;
            head.next =pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    public static void printNode(Node head){
        while(head != null){
            System.out.print(head.value + " ");
            head = head.next;
        }
    }

    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3= new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        n1.next=n2;
        n1.next.next = n3;
        n1.next.next.next = n4;
        n1.next.next.next.next = n5;
        printNode(n1);
        Node head = reversalListNode(n1);
        System.out.println();
        printNode(head);

    }
}
